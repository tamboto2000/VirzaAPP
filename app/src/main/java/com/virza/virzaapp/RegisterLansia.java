package com.virza.virzaapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class RegisterLansia extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daftar_lansia);

        final EditText dateTimePick = findViewById(R.id.daftarInputDob);

        dateTimePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(RegisterLansia.this);
                dpd.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        dateTimePick.setText("" + year + "-" + month + "-" + dayOfMonth);
                    }
                });

                dpd.show();
            }
        });

        Button reg = findViewById(R.id.buttonRegisterLansia);
        ProgressBar prog = findViewById(R.id.progressBar);
        EditText name = findViewById(R.id.daftarInputNama);
        EditText phone = findViewById(R.id.daftarInputHp);
        EditText address = findViewById(R.id.daftarInputAlamat);
        EditText password = findViewById(R.id.daftarInputPass);

        register(reg, prog, name, phone, dateTimePick, address, password);
    }

    private void register(Button btn, final ProgressBar prog, final EditText name,
                          final EditText phone, final EditText dob, final EditText address,
                          final EditText password) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("") || phone.getText().toString().equals("") ||
                        dob.getText().toString().equals("") || address.getText().toString().equals("") ||
                password.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Mohon isi semua data", Toast.LENGTH_LONG);
                    toast.show();

                    return;
                }

                String url = "http://31.220.61.116:8080/register/lansia";

                prog.setVisibility(View.VISIBLE);
                RequestQueue queue = Volley.newRequestQueue(RegisterLansia.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                prog.setVisibility(View.GONE);
                                Log.d("Response", response);
                                Intent intent = new Intent(RegisterLansia.this, LoginLansia.class);
                                startActivity(intent);
                                finish();
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                prog.setVisibility(View.GONE);
                                Log.d("Error.Response", error.getMessage());
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("name", name.getText().toString());
                        params.put("phone", phone.getText().toString());
                        params.put("dob", dob.getText().toString());
                        params.put("address", address.getText().toString());
                        params.put("password", password.getText().toString());

                        return params;
                    }
                };

                queue.add(postRequest);
            }
        });
    }
}
