package com.virza.virzaapp.volunteer;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.virza.virzaapp.EditProfile;
import com.virza.virzaapp.ImageFilePath;
import com.virza.virzaapp.R;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class SubmitArticle extends AppCompatActivity {
    private static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 2;
    private String encodedPict;
    private Button pickPicture;
    private EditText title;
    private EditText content;
    private EditText source;
    private Button submit;
    private ProgressBar pg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article);

        TextView profBtn = findViewById(R.id.profile);
        profBtn.setVisibility(TextView.GONE);

        TextView logoutBtn = findViewById(R.id.logout);
        logoutBtn.setVisibility(TextView.GONE);

        TextView topNavText = findViewById(R.id.topNavText);
        topNavText.setText("Tambah Artikel");

        pickPicture = findViewById(R.id.thumbnailInput);
        pickThumbnail(pickPicture);

        title = findViewById(R.id.titleInput);
        content = findViewById(R.id.contentInput);
        source = findViewById(R.id.sourceInput);
        submit = findViewById(R.id.submit);
        pg = findViewById(R.id.progressBar);

        submitArticle(submit);
    }

    private void submitArticle(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String title_str = title.getText().toString();
                final String content_str = content.getText().toString();
                final String source_str = source.getText().toString();

                if (title_str.isEmpty() || content_str.isEmpty() || source_str.isEmpty()
                || encodedPict == null) {
                    Toast.makeText(SubmitArticle.this,
                            "Mohon lengkapi data",
                            Toast.LENGTH_LONG).show();

                    return;
                }

                RequestQueue queue = Volley.newRequestQueue(SubmitArticle.this);
                String url = "http://31.220.61.116:8080/post/volunteer/article/create";
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pg.setVisibility(ProgressBar.GONE);
                                        Toast.makeText(SubmitArticle.this,
                                                "Berhasil mengunggah artikel",
                                                Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(final VolleyError e) {
                                // error
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pg.setVisibility(ProgressBar.GONE);
                                        Toast.makeText(SubmitArticle.this,
                                                "Terjadi kesalahan: "+e.getMessage(),
                                                Toast.LENGTH_LONG)
                                                .show();
                                    }
                                });
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("title", title_str);
                        params.put("content", content_str);
                        params.put("source", source_str);
                        params.put("thumbnail", encodedPict);

                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(SubmitArticle.this);
                        Map<String, String>  params = new HashMap<>();
                        params.put("user-id", sp.getString("userId", ""));
                        params.put("token-id", sp.getString("tokenId", ""));
                        params.put("token", sp.getString("token", ""));

                        return params;
                    }
                };

                queue.add(postRequest);
            }
        });
    }

    private void pickThumbnail(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int writeExternalStoragePermission = ContextCompat.checkSelfPermission(SubmitArticle.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if(writeExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
                {
                    // Request user to grant write external storage permission.
                    ActivityCompat.requestPermissions(SubmitArticle.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
                } else {
                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(pickPhoto , 1);
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            int grantResultsLength = grantResults.length;
            if (grantResultsLength > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    String selectedImage = ImageFilePath.getPath(SubmitArticle.this, imageReturnedIntent.getData());
                    Bitmap drawable = BitmapFactory.decodeFile(selectedImage);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    drawable.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] imageBytes = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                    encodedPict = encodedImage;

                    pickPicture.setTextSize(10);
                    pickPicture.setText(selectedImage);
                }

                break;
        }
    }
}
