package com.virza.virzaapp.volunteer;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.virza.virzaapp.MainActivity;
import com.virza.virzaapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HomeVolunteer extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_volunteer);

        ProgressBar prog = findViewById(R.id.progressBar);
        TextView logout = findViewById(R.id.logout);

        TextView profile = findViewById(R.id.profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeVolunteer.this, EditProfileVolunteer.class);
                startActivity(intent);
            }
        });

        final ImageView profilePict = findViewById(R.id.profilePict);

        logout(logout, prog);

        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(HomeVolunteer.this);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Bitmap drawable = BitmapFactory.decodeStream(new URL("http://31.220.61.116:8080"+
                            sp.getString("profilePict", "")).
                            openConnection().
                            getInputStream());

                    final RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),
                            drawable);
                    roundedBitmapDrawable.setCircular(true);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            profilePict.setImageDrawable(roundedBitmapDrawable);
                        }
                    });
                } catch (final MalformedURLException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(HomeVolunteer.this, "Terjadi kesalahan: "+e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                } catch (final IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(HomeVolunteer.this, "Terjadi kesalahan: "+e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        Button todoList = findViewById(R.id.btnTodoList);
        todoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeVolunteer.this, TodoList.class);
                startActivity(intent);
            }
        });

        thread.start();

        Button addArticle = findViewById(R.id.btnPost);
        addArticle(addArticle);
    }

    private void addArticle(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeVolunteer.this, SubmitArticle.class);
                startActivity(intent);
            }
        });
    }

    private void logout(TextView logout, final ProgressBar prog) {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(HomeVolunteer.this);
                final SharedPreferences.Editor editor = sp.edit();

                String url = "http://31.220.61.116:8080/logout/volunteer";
                final String tokenId = sp.getString("tokenId", null);

                prog.setVisibility(View.VISIBLE);

                RequestQueue queue = Volley.newRequestQueue(HomeVolunteer.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("code").equals("200")) {
                                        Toast toast = Toast.makeText(getApplicationContext(),
                                                "berhasil keluar", Toast.LENGTH_LONG);
                                        toast.show();
                                    }

                                } catch (JSONException e) {
                                    Toast toast = Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG);
                                    toast.show();
                                }

                                prog.setVisibility(View.GONE);

                                editor.clear();
                                editor.apply();

                                Intent intent = new Intent(HomeVolunteer.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                prog.setVisibility(View.GONE);
                                Toast.makeText(HomeVolunteer.this,
                                        "Terjadi kesalahan, silahkan coba lagi",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("tokenId", tokenId);

                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
    }
}
