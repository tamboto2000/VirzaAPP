package com.virza.virzaapp.volunteer;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.virza.virzaapp.HomeLansia;
import com.virza.virzaapp.LoginLansia;
import com.virza.virzaapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;

public class LoginVolunteer extends AppCompatActivity {
    private EditText username;
    private EditText password;
    private ProgressBar prog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_volunteer);

        username = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        prog = findViewById(R.id.progressBar);

        Button daftarVolunteer = findViewById(R.id.btnRegister);
        Button login = findViewById(R.id.btnLogin);

        daftarVolunteer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDaftarVolun = new Intent(LoginVolunteer.this, RegisterVolunteer.class);
                startActivity(intentDaftarVolun);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeVolunteer = new Intent(LoginVolunteer.this, HomeVolunteer.class);
                startActivity(homeVolunteer);
            }
        });

        login(login);
    }

    private void login(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isInternetAvailable()) {
                    Toast toast = Toast.makeText(LoginVolunteer.this, "Tidak ada sambungan ke internet", Toast.LENGTH_LONG);
                    toast.show();

                    return;
                }

                String url = "http://31.220.61.116:8080/login/volunteer";

                prog.setBackground(new ColorDrawable(Color.TRANSPARENT));

                prog.setVisibility(View.VISIBLE);
                RequestQueue queue = Volley.newRequestQueue(LoginVolunteer.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);

                                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(LoginVolunteer.this);
                                    SharedPreferences.Editor editor = sp.edit();

                                    editor.putString("role", "volunteer");
                                    editor.putString("tokenId", jsonObject.getString("tokenId"));
                                    editor.putString("token", jsonObject.getString("token"));
                                    editor.putString("userId", jsonObject.getString("userId"));
                                    editor.apply();

//                                    getProfile();
                                } catch (JSONException e) {
                                    Log.d("LoginVolunteer", e.getMessage());
                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            "Terjadi kesalahan: "+e.getMessage(),
                                            Toast.LENGTH_LONG);
                                    toast.show();
                                }

                                getProfile(prog);
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                prog.setVisibility(View.GONE);
                                Toast.makeText(LoginVolunteer.this, "Nomor HP atau password salah", Toast.LENGTH_LONG).
                                        show();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("username", username.getText().toString());
                        params.put("password", password.getText().toString());

                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
    }

    private boolean isInternetAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            if (!activeNetworkInfo.isConnected() || !activeNetworkInfo.isAvailable()) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    private void getProfile(final ProgressBar prog) {
        String url = "http://31.220.61.116:8080/profile/volunteer/get";
        OkHttpClient client = new OkHttpClient();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(LoginVolunteer.this);
        final SharedPreferences.Editor editor = sp.edit();

        okhttp3.Request request = new okhttp3.Request.Builder().
                header("user-id", sp.getString("userId", "")).
                header("token-id", sp.getString("tokenId", "")).
                header("token", sp.getString("token", "")).
                url(url).
                build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("LoginVolunteer.java", e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                String rawData = response.body().string();
                Log.d("LoginVolunteer", rawData);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        prog.setVisibility(ProgressBar.GONE);
                    }
                });

                try {
                    JSONObject jsonObject = new JSONObject(rawData);

                    editor.putString("name", jsonObject.getString("name"));
                    editor.putString("phone", jsonObject.getString("phone"));
                    editor.putString("email", jsonObject.getString("email"));
                    editor.putString("profilePict", jsonObject.getString("profilePict"));
                    editor.putString("dob", jsonObject.getString("dob"));
                    editor.putString("address", jsonObject.getString("address"));
                    editor.putString("profCreatedAt", jsonObject.getString("createdAt"));
                    editor.putString("profUpdatedAt", jsonObject.getString("updatedAt"));
                    editor.apply();
                } catch (final JSONException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("LoginVolunteer", e.getMessage());
                            Toast.makeText(LoginVolunteer.this, "Terjadi kesalahan: "+e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }

                Intent intent = new Intent(LoginVolunteer.this, HomeVolunteer.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
