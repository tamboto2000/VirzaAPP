package com.virza.virzaapp.volunteer;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.virza.virzaapp.LoginLansia;
import com.virza.virzaapp.R;
import com.virza.virzaapp.RegisterLansia;

import java.util.HashMap;
import java.util.Map;

public class RegisterVolunteer extends AppCompatActivity {
    private EditText dateTimePick;
    private Button reg;
    private ProgressBar prog;
    private EditText name;
    private EditText phone;
    private EditText address;
    private EditText password;
    private EditText email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daftar_volunteer);

        dateTimePick = findViewById(R.id.dob);

        dateTimePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dpd = new DatePickerDialog(RegisterVolunteer.this);
                dpd.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        dateTimePick.setText("" + year + "-" + month + "-" + dayOfMonth);
                    }
                });

                dpd.show();
            }
        });

        reg = findViewById(R.id.btnRegist);
        prog = findViewById(R.id.progressBar);
        name = findViewById(R.id.fullName);
        phone = findViewById(R.id.phoneNumber);
        address = findViewById(R.id.address);
        password = findViewById(R.id.password);
        email = findViewById(R.id.email);

        register(reg);
    }

    private void register(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (name.getText().toString().equals("") || phone.getText().toString().equals("") ||
                        dateTimePick.getText().toString().equals("") || address.getText().toString().equals("") ||
                        password.getText().toString().equals("")) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Mohon isi semua data", Toast.LENGTH_LONG);
                    toast.show();

                    return;
                }

                String url = "http://31.220.61.116:8080/register/volunteer";

                prog.setVisibility(View.VISIBLE);
                RequestQueue queue = Volley.newRequestQueue(RegisterVolunteer.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                prog.setVisibility(View.GONE);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(RegisterVolunteer.this,
                                                "Berhasil terdaftar",
                                                Toast.LENGTH_LONG).show();
                                    }
                                });

                                Log.d("Response", response);
                                Intent intent = new Intent(RegisterVolunteer.this, LoginVolunteer.class);
                                startActivity(intent);
                                finish();
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(final VolleyError error) {
                                // error
                                prog.setVisibility(View.GONE);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(RegisterVolunteer.this,
                                                "Terjadi kesalahan: "+error.getMessage(),
                                                Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("name", name.getText().toString());
                        params.put("phone", phone.getText().toString());
                        params.put("dob", dateTimePick.getText().toString());
                        params.put("address", address.getText().toString());
                        params.put("password", password.getText().toString());
                        params.put("email", email.getText().toString());

                        return params;
                    }
                };

                queue.add(postRequest);
            }
        });
    }
}
