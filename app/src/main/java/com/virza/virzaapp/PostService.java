package com.virza.virzaapp;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.net.URI;
import java.net.URISyntaxException;

import tech.gusavila92.websocketclient.WebSocketClient;

public class PostService extends IntentService {

    public static final int SUCCESS = 2;
    public static final int ERROR = 3;

    public PostService() {
        super(PostService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();

        openSocket(receiver, bundle);
    }

    private WebSocketClient webSocketClient;

    private void openSocket(final ResultReceiver receiver, final Bundle bundle) {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(PostService.this);

        URI uri;
        String url = "ws://31.220.61.116:8080/socket/connect?id="+sp.getString("userId","")+
                "&group=main";
        try {
            uri = new URI(url);
        }
        catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        webSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen() {

            }

            @Override
            public void onTextReceived(String message) {
//                Toast toast = Toast.makeText(PostService.this, message, Toast.LENGTH_LONG);
                System.out.println(message);
                bundle.putString("data", message);
                receiver.send(PostService.SUCCESS, bundle);
//                toast.show();
            }

            @Override
            public void onBinaryReceived(byte[] data) {

            }

            @Override
            public void onPingReceived(byte[] data) {

            }

            @Override
            public void onPongReceived(byte[] data) {

            }

            @Override
            public void onException(Exception e) {
                System.out.println(e.getMessage());
            }

            @Override
            public void onCloseReceived() {

            }
        };

        webSocketClient.setConnectTimeout(10000);
        webSocketClient.setReadTimeout(60000);
        webSocketClient.addHeader("token-id", sp.getString("tokenId", ""));
        webSocketClient.addHeader("token", sp.getString("token", ""));
        webSocketClient.addHeader("user-id", sp.getString("userId", ""));
        webSocketClient.enableAutomaticReconnection(5000);
        webSocketClient.connect();
    }
}

//    private OkHttpClient webSocketClient = new OkHttpClient();
//
//    private void openSocket() {
//        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(Komunitas.this);
//
//        String url = "ws://31.220.61.116:8080/socket/connect?id="+sp.getString("userId","")+
//                "&group=main";
//
//        Request request = new Request.Builder().
//                addHeader("user-id", sp.getString("userId", "")).
//                addHeader("token-id", sp.getString("tokenId", "")).
//                addHeader("token", sp.getString("token", "")).
//                url(url).build();
//
//        WebSocketListener webSocketListener = new WebSocketListener() {
//            @Override
//            public void onOpen(WebSocket webSocket, Response response) {
//                super.onOpen(webSocket, response);
//
//                Log.d("SocketMessage", "socket opened");
//            }
//
//            @Override
//            public void onMessage(WebSocket webSocket, String text) {
//                Log.d("SocketMessage", text);
//            }
//            @Override
//            public void onMessage(WebSocket webSocket, ByteString bytes) {
//                Log.d("SocketMessage", bytes.hex());
//            }
//
//
//            @Override
//            public void onFailure(WebSocket webSocket, Throwable t, Response response) {
//                Log.d("SocketError", t.getMessage());
//            }
//
//            @Override
//            public void onClosed(WebSocket webSocket, int code, String reason) {
//                Log.d("SocketMessage", "socket closed");
//            }
//        };
//
//        this.webSocketClient.newWebSocket(request, webSocketListener);
//        this.webSocketClient.dispatcher().executorService().shutdown();
//    }
