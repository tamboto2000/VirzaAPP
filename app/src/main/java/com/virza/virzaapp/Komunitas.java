package com.virza.virzaapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Komunitas extends AppCompatActivity {
    private List<String> name;
    private List<String> info;
    private List<RoundedBitmapDrawable> pp;
    private List<String> comment;
    private Button btnMore;
    private ProgressBar pg;
    private String postCursor;
    private String postCount;
    boolean headLoaded = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.komunitas);

        name = new ArrayList<>();
        info = new ArrayList<>();
        pp = new ArrayList<>();
        comment = new ArrayList<>();

        //Inflate header and footer
        View postLisFooter = getLayoutInflater().inflate(R.layout.post_list_footer, null);
        final View postListHeader = getLayoutInflater().inflate(R.layout.post_list_header, null);

        //Get button and progress bar from footer
        btnMore = postLisFooter.findViewById(R.id.btnMore);
        pg = postLisFooter.findViewById(R.id.progressBar);

        //Get button, progress bar, and editText from Header
        final Button btnPost = postListHeader.findViewById(R.id.btnPost);
        final ProgressBar headerPg = postListHeader.findViewById(R.id.progressBar);
        final EditText editText = postListHeader.findViewById(R.id.postText);
        final ImageView profilePict = postListHeader.findViewById(R.id.profilePict);

        //Set adapter to ListView
        final PostListAdapter postListAdapter = new PostListAdapter(this, name, info, pp, comment);
        ListView postList = findViewById(R.id.postList);

        postList.setAdapter(postListAdapter);

        //Set footer and header
        postList.addHeaderView(postListHeader);
        postList.addFooterView(postLisFooter);

        Button post = findViewById(R.id.btnTopik);
        post(post);

        //Load more
        btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final HomeFeed homeFeed = new HomeFeed();
                homeFeed.postListAdapter = postListAdapter;
                homeFeed.name = name;
                homeFeed.info = info;
                homeFeed.pp = pp;
                homeFeed.comment = comment;

                btnMore.setVisibility(Button.GONE);
                btnMore.setEnabled(false);
                pg.setVisibility(ProgressBar.VISIBLE);

                homeFeed.execute();
            }
        });

        //First load
        final HomeFeed homeFeed = new HomeFeed();
        homeFeed.postListAdapter = postListAdapter;
        homeFeed.name = name;
        homeFeed.info = info;
        homeFeed.pp = pp;
        homeFeed.comment = comment;
        homeFeed.profilePict = profilePict;
        btnMore.setVisibility(Button.GONE);
        btnMore.setEnabled(false);
        pg.setVisibility(ProgressBar.VISIBLE);
        homeFeed.execute();

        postFromHeader(btnPost, headerPg, editText);
    }

    public class HomeFeed extends AsyncTask<Void, Void, String> {
        public List<String> name;
        public List<String> info;
        public List<RoundedBitmapDrawable> pp;
        public List<String> comment;
        ImageView profilePict;

        PostListAdapter postListAdapter;

        @Override
        protected String doInBackground(Void... params) {
            homeFeed(name, info, pp, comment);
            final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(Komunitas.this);
            if(!sp.getString("profilePict", "").equals("")) {
                if(!headLoaded) {
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Bitmap drawable = BitmapFactory.decodeStream(new URL("http://31.220.61.116:8080"+
                                        sp.getString("profilePict", "")).
                                        openConnection().
                                        getInputStream());

                                final RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),
                                        drawable);
                                roundedBitmapDrawable.setCircular(true);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        profilePict.setImageDrawable(roundedBitmapDrawable);
                                        headLoaded = true;
                                    }
                                });
                            } catch (MalformedURLException e) {
                                Log.d("EditProfile.java", e.getMessage());
                            } catch (IOException e) {
                                Log.d("EditProfile.java", e.getMessage());
                            }
                        }
                    };

                    thread.start();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            btnMore.setVisibility(Button.VISIBLE);
            btnMore.setEnabled(true);
            pg.setVisibility(ProgressBar.GONE);

            postListAdapter.notifyDataSetChanged();
        }
    }

    private void postFromHeader(final Button btn, final ProgressBar pg, final EditText editText) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn.setVisibility(Button.GONE);
                btn.setEnabled(false);
                editText.setVisibility(EditText.GONE);
                editText.setEnabled(false);

                sendPost(btn, pg, editText);
            }
        });
    }

    private void post(final Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Komunitas.this, PostLansia.class);
                startActivity(intent);
            }
        });
    }

    private void homeFeed(final List<String> name, final List<String> info, final List<RoundedBitmapDrawable> pp,
                          final List<String> comment) {
        new Thread() {
            public void run() {
                String rawData = getHomeFeedData();
                if (rawData.equals("")) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Belum ada unggahan untuk ditampilkan, " +
                                    "tambahkan teman untuk melihat unggahan",
                            Toast.LENGTH_LONG);
                    toast.show();
                    return;
                }

                try {
                    JSONObject jsonObject = new JSONObject(rawData);
                    if (!jsonObject.getString("code").equals("200")) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Terjadi kesalahan",
                                Toast.LENGTH_LONG);
                        toast.show();
                        return;
                    }

                    if (jsonObject.getJSONObject("data").getJSONArray("post") == null || jsonObject.getJSONObject("data").getJSONArray("post").length() == 0) {
                        Toast toast = Toast.makeText(getApplicationContext(), "Belum ada unggahan untuk ditampilkan, " +
                                        "tambahkan teman untuk melihat unggahan",
                                Toast.LENGTH_LONG);
                        toast.show();
                    } else {
                        JSONArray posts = jsonObject.getJSONObject("data").getJSONArray("post");
                        postCursor = "cursor="+jsonObject.getJSONObject("data").getString("nextCursor");

                        for(int i=0; i<posts.length(); i++) {
                            name.add(posts.getJSONObject(i).getString("userName"));
                            info.add(posts.getJSONObject(i).getString("text"));
                            comment.add(posts.getJSONObject(i).getString("commentCount")+" komentar");

                            if (!posts.getJSONObject(i).getString("userPict").equals("")) {
                                try {
                                    Bitmap profilePict = BitmapFactory.decodeStream(new URL("http://31.220.61.116:8080"+posts.getJSONObject(i).
                                            getString("userPict")).
                                            openConnection().
                                            getInputStream());

                                    RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),
                                            profilePict);
                                    roundedBitmapDrawable.setCircular(true);
                                    pp.add(roundedBitmapDrawable);
                                } catch (MalformedURLException e) {
                                    Log.d("Komunitas.java", e.getMessage());
                                } catch (IOException e) {
                                    Log.d("Komunitas.java", e.getMessage());
                                }
                            } else {
                                try {
                                    Bitmap profilePict = BitmapFactory.decodeStream(new URL("http://31.220.61.116:8080/assets/default/user-image-with-black-background.png").
                                            openConnection().
                                            getInputStream());

                                    RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),
                                            profilePict);
                                    roundedBitmapDrawable.setCircular(true);
                                    pp.add(roundedBitmapDrawable);
                                } catch (MalformedURLException e) {
                                    Log.d("Komunitas.java", e.getMessage());
                                } catch (IOException e) {
                                    Log.d("Komunitas.java", e.getMessage());
                                }
                            }
                        }
                    }

                } catch (JSONException e) {
                    Log.d("JSONException", e.getMessage());
                }
            }
        }.run();
    }

    private String getHomeFeedData() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().
                header("user-id", sp.getString("userId", "")).
                header("token-id", sp.getString("tokenId", "")).
                header("token", sp.getString("token", "")).
                url("http://31.220.61.116:8080/post/lansia/homeFeed?"+postCursor+"&"+postCount).
                build();

        try {
            Response response = client.newCall(request).execute();
            return response.body().string();
        } catch (final IOException e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(Komunitas.this, "Terjadi kesalahan: "+e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        }

        return "";
    }

    private void sendPost(final Button btn, final ProgressBar prog, final EditText txt) {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(Komunitas.this);

        String url = "http://31.220.61.116:8080/post/lansia/create";

        prog.setVisibility(View.VISIBLE);

        RequestQueue queue = Volley.newRequestQueue(Komunitas.this);
        StringRequest postRequest = new StringRequest(com.android.volley.Request.Method.POST, url,
                new com.android.volley.Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("code").equals("200")) {
                                Toast toast = Toast.makeText(getApplicationContext(),
                                        "berhasil meng-unggah", Toast.LENGTH_LONG);
                                toast.show();
                            }

                        } catch (JSONException e) {
                            Toast toast = Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG);
                            toast.show();
                        }

                        prog.setVisibility(View.GONE);

                        btn.setVisibility(Button.VISIBLE);
                        btn.setEnabled(true);
                        txt.setVisibility(EditText.VISIBLE);
                        txt.setEnabled(true);
                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        prog.setVisibility(View.GONE);
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("text", txt.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("token-id", sp.getString("tokenId", ""));
                params.put("token", sp.getString("token", ""));
                params.put("user-id", sp.getString("userId", ""));

                return params;
            }
        };
        queue.add(postRequest);
    }
}
