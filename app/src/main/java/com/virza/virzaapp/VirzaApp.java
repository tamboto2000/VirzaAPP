package com.virza.virzaapp;

import android.app.Application;

public class VirzaApp extends Application {
    private String token;
    private String tokenId;
    private String userId;

    public String getToken() {
        return token;
    }

    public String getTokenId() {
        return tokenId;
    }

    public String getUserId() {
        return userId;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
