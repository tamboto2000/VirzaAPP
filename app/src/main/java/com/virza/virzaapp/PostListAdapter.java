package com.virza.virzaapp;

import android.app.Activity;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class PostListAdapter extends ArrayAdapter {
    private final Activity context;

    private final List<RoundedBitmapDrawable> imageIDArray;

    private final List<String> nameArray;

    private final List<String> infoArray;

    private final List<String> commentCount;

    public PostListAdapter(Activity context, List<String> nameArray, List<String> infoArray,
                           List<RoundedBitmapDrawable> imageIDArray, List<String> commentCount) {
        super(context, R.layout.post_row, nameArray);

        this.context = context;
        this.imageIDArray = imageIDArray;
        this.nameArray = nameArray;
        this.infoArray = infoArray;
        this.commentCount = commentCount;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.post_row, null,true);
        TextView name = rowView.findViewById(R.id.name);
        TextView info = rowView.findViewById(R.id.info);
        Button comment = rowView.findViewById(R.id.comment);
        ImageView pp = rowView.findViewById(R.id.profilePhoto);

        name.setText(nameArray.get(position));
        info.setText(infoArray.get(position));
        pp.setImageDrawable(imageIDArray.get(position));
        comment.setText(commentCount.get(position));

        return rowView;
    }
}
