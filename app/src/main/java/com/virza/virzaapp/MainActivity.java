package com.virza.virzaapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.virza.virzaapp.volunteer.HomeVolunteer;
import com.virza.virzaapp.volunteer.LoginVolunteer;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        Button btnLoginLansia = findViewById(R.id.actloginLansia);
        Button btnLoginVolunteer = findViewById(R.id.actloginVolunteer);
        loginLansia(btnLoginLansia);
        loginVolunteer(btnLoginVolunteer);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String userId = sp.getString("userId", null);
        String tokenId = sp.getString("tokenId", null);
        String token = sp.getString("token", null);
        String role = sp.getString("role", null);

        if (userId != null && tokenId != null && token != null && role != null) {
            if (role.equals("lansia")) {
                Intent intent = new Intent(this, HomeLansia.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(this, HomeVolunteer.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private void loginLansia(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginLansia.class);
                startActivity(intent);
            }
        });
    }

    private void loginVolunteer(Button btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginVolunteer.class);
                startActivity(intent);
            }
        });
    }
}

