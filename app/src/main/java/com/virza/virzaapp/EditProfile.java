package com.virza.virzaapp;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;

public class EditProfile extends AppCompatActivity {
    private static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 2;
    private ImageView profilePict;
    private String encodedPict;
    private ProgressBar pg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);

        profilePict = findViewById(R.id.profilePict);

        TextView navText = findViewById(R.id.topNavText);
        navText.setText("Ubah Profil");

        TextView logOut = findViewById(R.id.logout);
        pg = findViewById(R.id.progressBar);

        logout(logOut, pg);

        TextView editPicture = findViewById(R.id.editPicture);
        Button updateProfile = findViewById(R.id.btnUpdate);

        Profile profile = new Profile();
        profile.loadProfile();
        profile.updateProfilePict(editPicture);
        profile.updateProfile(updateProfile);
    }

    private class Profile {
        private EditText name;
        private EditText phone;
        private EditText password;
        private  EditText dob;
        private EditText address;

        public Profile() {
            name = findViewById(R.id.fullName);
            phone = findViewById(R.id.phoneNum);
            password = findViewById(R.id.password);
            dob = findViewById(R.id.dob);
            address = findViewById(R.id.address);
        }

        public void updateProfile(Button btn) {
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pg.setVisibility(ProgressBar.VISIBLE);

                    final String name_str = name.getText().toString();
                    final String phone_str = phone.getText().toString();
                    final String password_str = password.getText().toString();
                    final String dob_str = dob.getText().toString();
                    final String address_str = address.getText().toString();

                    if (name_str.isEmpty() || phone_str.isEmpty() || password_str.isEmpty() ||
                            dob_str.isEmpty() || address_str.isEmpty()) {
                        Toast.makeText(EditProfile.this, "Mohon lengkapi data",
                                Toast.LENGTH_LONG).show();

                        return;
                    }

                    RequestQueue queue = Volley.newRequestQueue(EditProfile.this);
                    String url = "http://31.220.61.116:8080/profile/lansia/update";
                    StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                            new Response.Listener<String>()
                            {
                                @Override
                                public void onResponse(String response) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            resetData();
                                            pg.setVisibility(ProgressBar.GONE);
                                        }
                                    });
                                }
                            },
                            new Response.ErrorListener()
                            {
                                @Override
                                public void onErrorResponse(final VolleyError e) {
                                    // error
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pg.setVisibility(ProgressBar.GONE);
                                            Toast.makeText(EditProfile.this,
                                                    "Terjadi kesalahan: "+e.getMessage(),
                                                    Toast.LENGTH_LONG)
                                                    .show();
                                        }
                                    });
                                }
                            }
                    ) {
                        @Override
                        protected Map<String, String> getParams()
                        {
                            Map<String, String>  params = new HashMap<>();
                            params.put("name", name_str);
                            params.put("phone", phone_str);
                            params.put("password", password_str);
                            params.put("dob", dob_str);
                            params.put("address", address_str);
                            if (encodedPict != null) {
                                params.put("profilePict", encodedPict);
                            }

                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(EditProfile.this);
                            Map<String, String>  params = new HashMap<>();
                            params.put("user-id", sp.getString("userId", ""));
                            params.put("token-id", sp.getString("tokenId", ""));
                            params.put("token", sp.getString("token", ""));

                            return params;
                        }
                    };

                    queue.add(postRequest);
                }
            });
        }

        public void loadProfile() {
            final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(EditProfile.this);

            name.setText(sp.getString("name", ""));
            phone.setText(sp.getString("phone", ""));
            password.setText(sp.getString("password", ""));
            dob.setText(sp.getString("dob", ""));
            address.setText(sp.getString("address", ""));

            LoadProfilePict loadProfilePict = new LoadProfilePict();
            loadProfilePict.execute();
        }

        private void resetData() {
            String url = "http://31.220.61.116:8080/profile/lansia/get";
            OkHttpClient client = new OkHttpClient();
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(EditProfile.this);
            final SharedPreferences.Editor editor = sp.edit();

            okhttp3.Request request = new okhttp3.Request.Builder().
                    header("user-id", sp.getString("userId", "")).
                    header("token-id", sp.getString("tokenId", "")).
                    header("token", sp.getString("token", "")).
                    url(url).
                    build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.d("LoginLansia.java", e.getMessage());
                }

                @Override
                public void onResponse(Call call, okhttp3.Response response) throws IOException {
                    String rawData = response.body().string();

                    try {
                        JSONObject jsonObject = new JSONObject(rawData);

                        editor.putString("name", jsonObject.getString("name"));
                        editor.putString("phone", jsonObject.getString("phone"));
                        editor.putString("profilePict", jsonObject.getString("profilePict"));
                        editor.putString("dob", jsonObject.getString("dob"));
                        editor.putString("address", jsonObject.getString("address"));
                        editor.putString("profCreatedAt", jsonObject.getString("createdAt"));
                        editor.putString("profUpdatedAt", jsonObject.getString("updatedAt"));
                        editor.apply();

                       runOnUiThread(new Runnable() {
                           @Override
                           public void run() {
                               Toast.makeText(EditProfile.this,
                                       "Profil telah diperbarui",
                                       Toast.LENGTH_LONG)
                                       .show();
                           }
                       });
                    } catch (final JSONException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(EditProfile.this,
                                    "Profil telah diperbarui, namun terjadi kesalahan: "+e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                            }
                        });
                    }
                }
            });
        }

        private class LoadProfilePict extends AsyncTask<String, String, String> {
            final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(EditProfile.this);
            private RoundedBitmapDrawable roundedBitmapDrawable;

            @Override
            protected String doInBackground(String... strings) {
                if (!sp.getString("profilePict", "").equals("")) {
                    try {
                        Bitmap drawable = BitmapFactory.decodeStream(new URL("http://31.220.61.116:8080"+
                                sp.getString("profilePict", "")).
                                openConnection().
                                getInputStream());

                        roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),
                                drawable);
                        roundedBitmapDrawable.setCircular(true);
                    } catch (MalformedURLException e) {
                        Log.d("EditProfile.java", e.getMessage());
                    } catch (IOException e) {
                        Log.d("EditProfile.java", e.getMessage());
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if (roundedBitmapDrawable != null) {
                    profilePict.setImageDrawable(roundedBitmapDrawable);
                }
            }
        }

        public void updateProfilePict(TextView txt) {
            txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int writeExternalStoragePermission = ContextCompat.checkSelfPermission(EditProfile.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if(writeExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
                    {
                        // Request user to grant write external storage permission.
                        ActivityCompat.requestPermissions(EditProfile.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
                    } else {
                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(pickPhoto , 1);
                    }
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            int grantResultsLength = grantResults.length;
            if (grantResultsLength > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    String selectedImage = ImageFilePath.getPath(EditProfile.this, imageReturnedIntent.getData());
                    RoundedBitmapDrawable roundedBitmapDrawable;
                    Bitmap drawable = BitmapFactory.decodeFile(selectedImage);

                    roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),
                            drawable);
                    roundedBitmapDrawable.setCircular(true);
                    profilePict.setImageDrawable(roundedBitmapDrawable);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    drawable.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] imageBytes = baos.toByteArray();
                    String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                    encodedPict = encodedImage;

                    Log.d("EditProfile", encodedImage);
                }
                break;
        }
    }

    private void logout(TextView logout, final ProgressBar prog) {
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(EditProfile.this);
                final SharedPreferences.Editor editor = sp.edit();

                String url = "http://31.220.61.116:8080/logout/lansia";
                final String tokenId = sp.getString("tokenId", null);

                prog.setVisibility(View.VISIBLE);

                RequestQueue queue = Volley.newRequestQueue(EditProfile.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                Log.d("Response", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("code").equals("200")) {
                                        Toast toast = Toast.makeText(getApplicationContext(),
                                                "berhasil keluar", Toast.LENGTH_LONG);
                                        toast.show();
                                    }

                                } catch (JSONException e) {
                                    Toast toast = Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG);
                                    toast.show();
                                }

                                prog.setVisibility(View.GONE);

                                editor.clear();
                                editor.apply();

                                Intent intent = new Intent(EditProfile.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                prog.setVisibility(View.GONE);
                                Log.d("Error.Response", error.getMessage());
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("tokenId", tokenId);

                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
    }
}
