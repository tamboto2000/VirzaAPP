package com.virza.virzaapp;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class PostLansia extends AppCompatActivity {
    ImageView profilePict;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_lansia);
        TextView topNavText = findViewById(R.id.topNavText);
        char[] txt = {'U','n','g','g','a','h'};
        topNavText.setText(txt, 0, 6);

        Button btnPost = findViewById(R.id.btnPost);
        final EditText postText = findViewById(R.id.postText);
        final ProgressBar prog = findViewById(R.id.progressBar);

        profilePict = findViewById(R.id.profilePhoto);

        btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post(prog, postText);
            }
        });

        LoadProfilePict loadProfilePict = new LoadProfilePict();
        loadProfilePict.execute();
    }

    public void post(final ProgressBar prog, final EditText txt) {
        final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(PostLansia.this);

        String url = "http://31.220.61.116:8080/post/lansia/create";

        prog.setVisibility(View.VISIBLE);

        RequestQueue queue = Volley.newRequestQueue(PostLansia.this);
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("code").equals("200")) {
                                Toast toast = Toast.makeText(getApplicationContext(),
                                        "berhasil meng-unggah", Toast.LENGTH_LONG);
                                toast.show();
                            }

                        } catch (JSONException e) {
                            Toast toast = Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG);
                            toast.show();
                        }

                        prog.setVisibility(View.GONE);
                        finish();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        prog.setVisibility(View.GONE);
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                params.put("text", txt.getText().toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                params.put("token-id", sp.getString("tokenId", ""));
                params.put("token", sp.getString("token", ""));
                params.put("user-id", sp.getString("userId", ""));

                return params;
            }
        };
        queue.add(postRequest);
    }

    public class LoadProfilePict extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(PostLansia.this);
            if(!sp.getString("profilePict", "").equals("")) {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            Bitmap drawable = BitmapFactory.decodeStream(new URL("http://31.220.61.116:8080"+
                                    sp.getString("profilePict", "")).
                                    openConnection().
                                    getInputStream());

                            final RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),
                                    drawable);
                            roundedBitmapDrawable.setCircular(true);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    profilePict.setImageDrawable(roundedBitmapDrawable);
                                }
                            });
                        } catch (MalformedURLException e) {
                            Log.d("EditProfile.java", e.getMessage());
                        } catch (IOException e) {
                            Log.d("EditProfile.java", e.getMessage());
                        }
                    }
                };

                thread.start();
            }

            return null;
        }
    }
}
