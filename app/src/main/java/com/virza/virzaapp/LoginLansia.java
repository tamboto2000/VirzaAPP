package com.virza.virzaapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;

public class LoginLansia extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_lansia);

        Button daftar = findViewById(R.id.btnRegister);
        Button btnLogin = findViewById(R.id.buttonLoginLansia);
        EditText username = findViewById(R.id.loginInputHP);
        EditText password = findViewById(R.id.loginInputPass);
        ProgressBar prog = findViewById(R.id.progressBar);

        daftarLansia(daftar);
        login(btnLogin, username, password, prog);
    }

    private void daftarLansia(Button txt) {
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentDaftarLansia = new Intent(LoginLansia.this, RegisterLansia.class);
                startActivity(intentDaftarLansia);
            }
        });
    }

    private void login(Button btn, final EditText usr, final EditText pass, final ProgressBar prog) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isInternetAvailable()) {
                    Toast toast = Toast.makeText(LoginLansia.this, "Tidak ada sambungan ke internet", Toast.LENGTH_LONG);
                    toast.show();

                    return;
                }

                String url = "http://31.220.61.116:8080/login/lansia";
                final String username = usr.getText().toString();
                final String password = pass.getText().toString();

                prog.setBackground(new ColorDrawable(Color.TRANSPARENT));

                prog.setVisibility(View.VISIBLE);
                RequestQueue queue = Volley.newRequestQueue(LoginLansia.this);
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                Log.d("Response", response);

                                try {
                                    JSONObject jsonObject = new JSONObject(response);

                                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(LoginLansia.this);
                                    SharedPreferences.Editor editor = sp.edit();

                                    editor.putString("role", "lansia");
                                    editor.putString("tokenId", jsonObject.getString("tokenId"));
                                    editor.putString("token", jsonObject.getString("token"));
                                    editor.putString("userId", jsonObject.getString("userId"));
                                    editor.apply();
                                } catch (JSONException e) {
                                    prog.setVisibility(View.GONE);
                                    Toast toast = Toast.makeText(getApplicationContext(),
                                            "Terjadi kesalahan"+e.getMessage(),
                                            Toast.LENGTH_LONG);
                                    toast.show();
                                }

                                getProfile(prog);
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                prog.setVisibility(View.GONE);
                                Toast.makeText(LoginLansia.this, "Nomor HP atau password salah", Toast.LENGTH_LONG).
                                    show();
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<>();
                        params.put("username", username);
                        params.put("password", password);

                        return params;
                    }
                };
                queue.add(postRequest);
            }
        });
    }

    private void getProfile(final ProgressBar prog) {
        String url = "http://31.220.61.116:8080/profile/lansia/get";
        OkHttpClient client = new OkHttpClient();
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(LoginLansia.this);
        final SharedPreferences.Editor editor = sp.edit();

        okhttp3.Request request = new okhttp3.Request.Builder().
                header("user-id", sp.getString("userId", "")).
                header("token-id", sp.getString("tokenId", "")).
                header("token", sp.getString("token", "")).
                url(url).
                build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("LoginLansia.java", e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                String rawData = response.body().string();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        prog.setVisibility(ProgressBar.GONE);
                    }
                });

                try {
                    JSONObject jsonObject = new JSONObject(rawData);

                    editor.putString("name", jsonObject.getString("name"));
                    editor.putString("phone", jsonObject.getString("phone"));
                    editor.putString("profilePict", jsonObject.getString("profilePict"));
                    editor.putString("dob", jsonObject.getString("dob"));
                    editor.putString("address", jsonObject.getString("address"));
                    editor.putString("profCreatedAt", jsonObject.getString("createdAt"));
                    editor.putString("profUpdatedAt", jsonObject.getString("updatedAt"));
                    editor.apply();
                } catch (final JSONException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(LoginLansia.this, "Terjadi kesalahan: "+e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }

                Intent intent = new Intent(LoginLansia.this, HomeLansia.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private boolean isInternetAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null) {
            if (!activeNetworkInfo.isConnected() || !activeNetworkInfo.isAvailable()) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }
}
